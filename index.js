const express = require('express');
const bodyParser = require('body-parser');

const users = [{id:0, name: 'admin'}];

const server = express();
server.use(bodyParser.json());

server.get('/users', (_, response) => response.send({ users }));
server.get('/users/:id', ({params:{id}}, response) => {
    const user = users[id];
    response.send({user})
})
server.post('/users', ({body}, response) =>  {
    const {user} = body;

    if (!(user instanceof Object))
        return response.send({ error: '"user" object missing in JSON!!!' });

    user.id = users.length;
    users.push(user);
    response.send({user});
})

server.listen(9999, () => {
    console.log('API running at http://localhost:9999')
});